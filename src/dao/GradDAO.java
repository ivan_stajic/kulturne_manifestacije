package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Grad;

public class GradDAO {
	
	public static List<Grad> getAll(Connection conn){
		List<Grad> gradovi = new ArrayList<>();
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT * FROM grad";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int ptt = rset.getInt(index++);
				String naziv = rset.getString(index++);
				
				Grad grad = new Grad(id, ptt, naziv);
				gradovi.add(grad);
			}
			
			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return gradovi;
	}
	
	public static Grad getGradById(Connection conn, int id) {
		Grad grad = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, pttBroj, naziv FROM grad WHERE id = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int idG = rset.getInt(index++);
				int pttBroj = rset.getInt(index++);
				String naziv = rset.getString(index++);
				
				grad = new Grad(idG, pttBroj, naziv);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return grad;
	}

	
	public static Grad getGradByPtt(Connection conn, int ptt) {
		Grad grad = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, pttBroj, naziv FROM grad WHERE pttBroj = " + ptt;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				int pttBroj = rset.getInt(index++);
				String naziv = rset.getString(index++);
				
				grad = new Grad(id, pttBroj, naziv);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return grad;
	}
	
	public static Grad getGradByNaziv(Connection conn, String naziv) {
		Grad grad = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, pttBroj, naziv FROM grad WHERE naziv = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, naziv);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				int pttB = rset.getInt(index++);
				String nazivG = rset.getString(index++);
				
				grad = new Grad(id, pttB, nazivG);
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return grad;
	}
	
	public static boolean add(Connection conn, Grad grad) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO grad (id, pttBroj, naziv) VALUES (?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, grad.getId());
			pstmt.setInt(index++, grad.getPttBroj());
			pstmt.setString(index++, grad.getNaziv());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	public static boolean update(Connection conn, Grad grad) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE grad SET pttBroj = ?, naziv = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setInt(index++, grad.getPttBroj());
			pstmt.setString(index++, grad.getNaziv());
			
			pstmt.setInt(index++, grad.getId());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	
	public static boolean delete(Connection conn, int id) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM grad WHERE id = " + id;

			stmt = conn.createStatement();
			return stmt.executeUpdate(update) == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}



}
