package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.Manifestacija;

public class ManifestacijaDAO {
	
	public static List<Manifestacija> getAll(Connection conn) {
		ArrayList<Manifestacija> manifestacije = new ArrayList<>();

		Statement stmt = null;
		ResultSet rset = null;

		try {
			String query = "SELECT * FROM manifestacija";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);

			while (rset.next()) {
				int index = 1;
				int id = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int brojPosetilaca = rset.getInt(index++);
				int idG = rset.getInt(index++);
				
				Manifestacija manifestacija = new Manifestacija(id, naziv, brojPosetilaca, GradDAO.getGradById(conn, idG));
				manifestacije.add(manifestacija);
				
			}

			rset.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return manifestacije;
	}
	
	public static Manifestacija getManifestacijaById(Connection conn, int id) {
		Manifestacija manifestacija = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, naziv, brojPosetilaca, gradOdrzavanja FROM manifestacija WHERE id = " + id;
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int idM = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int brojPos = rset.getInt(index++);
				int gradOd = rset.getInt(index++);
				
				
				manifestacija = new Manifestacija(idM, naziv, brojPos, GradDAO.getGradById(conn, gradOd));
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return manifestacija;
	}
	
	public static Manifestacija getManifestacijaWithMaxPeople(Connection conn) {
		Manifestacija manifestacija = null;
		
		Statement stmt = null;
		ResultSet rset = null;
		
		try {
			String query = "SELECT id, naziv, brojPosetilaca, gradOdrzavanja FROM manifestacija WHERE (brojPosetilaca) IN (SELECT MAX(brojPosetilaca) FROM manifestacija)";
			stmt = conn.createStatement();
			rset = stmt.executeQuery(query);
			
			while(rset.next()) {
				int index = 1;
				int idM = rset.getInt(index++);
				String naziv = rset.getString(index++);
				int brojPos = rset.getInt(index++);
				int gradOd = rset.getInt(index++);
				
				
				manifestacija = new Manifestacija(idM, naziv, brojPos, GradDAO.getGradById(conn, gradOd));
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}
		
		return manifestacija;
	}


	public static Manifestacija getManifestacijaByNaziv(Connection conn, String naziv) {
		Manifestacija manifestacija = null;

		PreparedStatement pstmt = null;
		ResultSet rset = null;
		try {
			String query = "SELECT id, naziv, brojPosetilaca, gradOdrzavanja FROM manifestacija WHERE naziv = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, naziv);

			rset = pstmt.executeQuery();

			if (rset.next()) {
				index = 1;
				int id = rset.getInt(index++);
				String nazivM = rset.getString(index++);
				int brojP = rset.getInt(index++);
				int gradO = rset.getInt(index++);
				
				manifestacija = new Manifestacija(id, nazivM, brojP, GradDAO.getGradById(conn, gradO));
			}
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
			try {rset.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return manifestacija;
	}
	
	public static boolean add(Connection conn, Manifestacija manifestacija) {
		PreparedStatement pstmt = null;
		try {
			String query = "INSERT INTO manifestacija (naziv, brojPosetilaca, gradOdrzavanja) VALUES (?, ?, ?)";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, manifestacija.getNaziv());
			pstmt.setInt(index++, manifestacija.getBrojPosetilaca());
			pstmt.setInt(index++, manifestacija.getGradOdrzavanja().getId());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}
	
	public static boolean update(Connection conn, Manifestacija manifestacija) {
		PreparedStatement pstmt = null;
		try {
			String query = "UPDATE manifestacija SET naziv = ?, brojPosetilaca = ?, gradOdrzavanja = ? WHERE id = ?";

			pstmt = conn.prepareStatement(query);
			int index = 1;
			pstmt.setString(index++, manifestacija.getNaziv());
			pstmt.setInt(index++, manifestacija.getBrojPosetilaca());
			pstmt.setInt(index++, manifestacija.getGradOdrzavanja().getId());

			pstmt.setInt(index++, manifestacija.getId());

			return pstmt.executeUpdate() == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {pstmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}

	public static boolean delete(Connection conn, int id) {
		Statement stmt = null;
		try {
			String update = "DELETE FROM manifestacija WHERE id = " + id;

			stmt = conn.createStatement();
			return stmt.executeUpdate(update) == 1;
		} catch (SQLException ex) {
			System.out.println("Greska u SQL upitu!");
			ex.printStackTrace();
		} finally {
			try {stmt.close();} catch (SQLException ex1) {ex1.printStackTrace();}
		}

		return false;
	}



}
