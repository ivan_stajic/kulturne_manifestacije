package model;

import java.util.ArrayList;

public class Grad {
	
	int id;
	int pttBroj;
	String naziv;
	ArrayList<Manifestacija> sveManifestacije = new ArrayList<Manifestacija>();
	
	public Grad(int id, int pttBroj, String naziv) {
		super();
		this.id = id;
		this.pttBroj = pttBroj;
		this.naziv = naziv;
	}

	@Override
	public String toString() {
		return "Grad [id=" + id + "pttBroj=" + pttBroj + ", naziv=" + naziv + "]";
	}
	
	public String toFormatedString() {
		return String.format("%-4s %8s %13s\n", id, pttBroj, naziv);
	}

	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Grad other = (Grad) obj;
		if (id != other.id)
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		if (pttBroj != other.pttBroj)
			return false;
		if (sveManifestacije == null) {
			if (other.sveManifestacije != null)
				return false;
		} else if (!sveManifestacije.equals(other.sveManifestacije))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getPttBroj() {
		return pttBroj;
	}

	public void setPttBroj(int pttBroj) {
		this.pttBroj = pttBroj;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public ArrayList<Manifestacija> getSveManifestacije() {
		return sveManifestacije;
	}

	public void setSveManifestacije(ArrayList<Manifestacija> sveManifestacije) {
		this.sveManifestacije = sveManifestacije;
	}
	

}
