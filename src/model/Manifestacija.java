package model;


public class Manifestacija {
	
	int id;
	String naziv;
	int brojPosetilaca;
	Grad gradOdrzavanja;
	
	
	public Manifestacija(int id, String naziv, int brojPosetilaca, Grad gradOdrzavanja) {
		super();
		this.id = id;
		this.naziv = naziv;
		this.brojPosetilaca = brojPosetilaca;
		this.gradOdrzavanja = gradOdrzavanja;
	}

	@Override
	public String toString() {
		return "Manifestacija [id=" + id + ", naziv=" + naziv + ", brojPosetilaca=" + brojPosetilaca
				+ ", gradOdrzavanja=" + gradOdrzavanja + "]";
	}
	
	public String toFormatedString() {
		return String.format("%-4d %-20s %10s %20s\n", id, naziv,
				brojPosetilaca, gradOdrzavanja.getNaziv());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Manifestacija other = (Manifestacija) obj;
		if (brojPosetilaca != other.brojPosetilaca)
			return false;
		if (gradOdrzavanja == null) {
			if (other.gradOdrzavanja != null)
				return false;
		} else if (!gradOdrzavanja.equals(other.gradOdrzavanja))
			return false;
		if (id != other.id)
			return false;
		if (naziv == null) {
			if (other.naziv != null)
				return false;
		} else if (!naziv.equals(other.naziv))
			return false;
		return true;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNaziv() {
		return naziv;
	}

	public void setNaziv(String naziv) {
		this.naziv = naziv;
	}

	public int getBrojPosetilaca() {
		return brojPosetilaca;
	}

	public void setBrojPosetilaca(int brojPosetilaca) {
		this.brojPosetilaca = brojPosetilaca;
	}

	public Grad getGradOdrzavanja() {
		return gradOdrzavanja;
	}

	public void setGradOdrzavanja(Grad gradOdrzavanja) {
		this.gradOdrzavanja = gradOdrzavanja;
	}

	

}
