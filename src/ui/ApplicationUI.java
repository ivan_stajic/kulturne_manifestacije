package ui;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import dao.ManifestacijaDAO;
import utils.PomocnaKlasa;


public class ApplicationUI {
	
private static Connection conn;
	
	static {
		// otvaranje konekcije, jednom na pocetku aplikacije
		try {
			// ucitavanje MySQL drajvera
			Class.forName("com.mysql.jdbc.Driver");
			// otvaranje konekcije
			conn = DriverManager.getConnection(
					"jdbc:mysql://localhost:3306/kulturnemanifestacije?useSSL=false", 
					"root", 
					"root");
		} catch (Exception ex) {
			System.out.println("Neuspela konekcija na bazu!");
			ex.printStackTrace();

			// kraj aplikacije
			System.exit(0);
		}
	}


	public static void main(String[] args) {

		int odluka = -1;
		while (odluka != 0) {
			ApplicationUI.ispisiMenu();
			
			System.out.print("opcija:");
			odluka = PomocnaKlasa.ocitajCeoBroj();
			
			switch (odluka) {
			case 0:
				System.out.println("Izlaz iz programa");
				break;
			case 1:
				GradUI.prikazSvihGradova();
				break;
			case 2:
				MonifestacijaUI.prikazSvihManifestacija(ManifestacijaDAO.getAll(conn));
				break;
			case 3:
				MonifestacijaUI.pronadjiManifestacijuPoID();
				break;
			case 4:
				MonifestacijaUI.izmeniNazivManifestacije();
				break;
			case 5:
				MonifestacijaUI.pronadjiManifestacijuSaNajvisePosetilaca();
				break;
			case 6:
				MonifestacijaUI.unosNoveManifestacije();
				break;
			case 7:
				MonifestacijaUI.promeniManifestaciju();
				break;
			case 8:
				MonifestacijaUI.obrisiManifestaciju();
				break;
			case 9:
				GradUI.unosNovogGrada();
				break;
			case 10:
				GradUI.promeniGrad();
				break;
			case 11:
				GradUI.obrisiGrad();
				break;

			default:
				System.out.println("Nepostojeca komanda");
				break;
			}
		}
		
		
		// zatvaranje konekcije, jednom na kraju aplikacije
		try {
			conn.close();
		} catch (SQLException ex) {
			ex.printStackTrace();
		}

	}
	
	// ispis teksta osnovnih opcija
	public static void ispisiMenu() {
		System.out.println("Kulturne Manifestacije - Osnovne opcije:");
		System.out.println("\tOpcija broj 1 - Prikaz gradova");
		System.out.println("\tOpcija broj 2 - Prikaz kulturnih manifestacija");
		System.out.println("\tOpcija broj 3 - Pretraga kulturne manifestacije po identifikatoru");
		System.out.println("\tOpcija broj 4 - Izmena naziva manifestacije");
		System.out.println("\tOpcija broj 5 - Prikaz manifestacije sa najvecim brojem posetilaca");
		System.out.println("\tOpcija broj 6 - Unos kulturne manifestacije");
		System.out.println("\tOpcija broj 7 - Izmena kulturne manifestacije");
		System.out.println("\tOpcija broj 8 - Brisanje kulturne manifestacije");
		System.out.println("\tOpcija broj 9 - Unos grada");
		System.out.println("\tOpcija broj 10 - Izmena grada");
		System.out.println("\tOpcija broj 11 - Brisanje grada");
		System.out.println("\t\t ...");
		System.out.println("\tOpcija broj 0 - IZLAZ IZ PROGRAMA");
	}

	public static Connection getConn() {
		return conn;
	}


}
