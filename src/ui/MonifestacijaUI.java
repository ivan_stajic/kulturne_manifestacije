package ui;

import java.util.List;

import dao.GradDAO;
import dao.ManifestacijaDAO;
import model.Grad;
import model.Manifestacija;
import utils.PomocnaKlasa;

public class MonifestacijaUI {
	
	public static void ispisZaglavlja() {
		System.out.println("\n=========================================================");
		System.out.printf("%-4s %-20s %10s %20s\n", "ID", "NAZIV", "BR.POSET.", "GRAD");
		System.out.println("=========================================================");

	}
	
	public static void prikazSvihManifestacija(List<Manifestacija> manifestacije) {
		ispisZaglavlja();
		for (Manifestacija man : manifestacije) {
			System.out.print(man.toFormatedString());
		}
		System.out.println("=========================================================\n");
	}
	
	public static void pronadjiManifestacijuPoID() {
		Manifestacija rez = null;
		
		while (rez == null) {
			System.out.println("Unesite id manifestacije: ");
			int trazeniID = PomocnaKlasa.ocitajCeoBroj();
			rez = ManifestacijaDAO.getManifestacijaById(ApplicationUI.getConn(), trazeniID);
		}
		ispisZaglavlja();
		System.out.println(rez.toFormatedString());
	}
	
	public static void pronadjiManifestacijuSaNajvisePosetilaca() {
		Manifestacija rez = ManifestacijaDAO.getManifestacijaWithMaxPeople(ApplicationUI.getConn());
		ispisZaglavlja();
		System.out.println(rez.toFormatedString());

	}
	
	public static void izmeniNazivManifestacije() {
		Manifestacija rez = null;
		
		while (rez == null) {
			System.out.println("Unesite id manifestacije za izmenu: ");
			int trazeniID = PomocnaKlasa.ocitajCeoBroj();
			rez = ManifestacijaDAO.getManifestacijaById(ApplicationUI.getConn(), trazeniID);
		}
		
		System.out.println("Unesite novi naziv manifestacije: ");
		String noviNaziv = PomocnaKlasa.ocitajTekst();
		
		rez = new Manifestacija (rez.getId(), noviNaziv, rez.getBrojPosetilaca(), rez.getGradOdrzavanja());
		System.out.println("Naziv manifestacije je uspesno promenjen.");
		
		System.out.println("\n=========================================================");
		System.out.printf("%-4s %-20s %10s %20s\n", "ID", "NAZIV", "BR.POSET.", "GRAD");
		System.out.println("=========================================================");
		System.out.println(rez.toFormatedString());
		
	}
	
	
	/** METODE ZA PRETRAGU MANIFESTACIJE ****/
	
	// unos nove manifestacije
	public static void unosNoveManifestacije() {
		System.out.print("Unesi naziv manifestacije:");
		String maNaziv = PomocnaKlasa.ocitajTekst();
		maNaziv = maNaziv.toUpperCase();
		while (pronadjiManifestaciju(maNaziv) != null) {
			System.out.println("Manifestacija pod nazivom " + maNaziv
					+ " vec postoji");
			maNaziv = PomocnaKlasa.ocitajTekst();
		}
		System.out.print("Unesi broj posetilaca:");
		int brojPos = PomocnaKlasa.ocitajCeoBroj();
		
		System.out.println("PRIKAZ SVIH GRADOVA:");
		GradUI.prikazSvihGradova();
		
		System.out.print("Unesi ptt grada:");
		int gradPtt = PomocnaKlasa.ocitajCeoBroj();

		
		Manifestacija ma = new Manifestacija(0, maNaziv, brojPos, GradDAO.getGradByPtt(ApplicationUI.getConn(), gradPtt));
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		ManifestacijaDAO.add(ApplicationUI.getConn(), ma);
	}
	
	// pronadji manifestaciju
	public static Manifestacija pronadjiManifestaciju() {
		Manifestacija retVal = null;
		System.out.print("Unesi naziv manifestacije:");
		String naziv = PomocnaKlasa.ocitajTekst();
		retVal = pronadjiManifestaciju(naziv);
		if (retVal == null)
			System.out.println("Manifestacija pod nazivom " + naziv
					+ " ne postoji u evidenciji");
		return retVal;
	}

	// pronadji manifestaciju
	public static Manifestacija pronadjiManifestaciju(String naziv) {
		Manifestacija retVal = ManifestacijaDAO.getManifestacijaByNaziv(ApplicationUI.getConn(),
				naziv);
		return retVal;
	}

	
	public static void promeniManifestaciju() {
		Manifestacija ma = null;
		while(ma == null) {
			ma = pronadjiManifestaciju();
		}

		System.out.print("Unesite novi naziv:");
		String maNaziv = PomocnaKlasa.ocitajTekst();
		
		System.out.print("Unesi broj posetilaca:");
		int brojPos = PomocnaKlasa.ocitajCeoBroj();
		
		System.out.println("PRIKAZ SVIH GRADOVA:");
		GradUI.prikazSvihGradova();
		
		System.out.print("Unesi ptt grada:");
		int gradPtt = PomocnaKlasa.ocitajCeoBroj();
		Grad grad = GradDAO.getGradByPtt(ApplicationUI.getConn(), gradPtt);

		ma.setNaziv(maNaziv);
		ma.setBrojPosetilaca(brojPos);
		ma.setGradOdrzavanja(grad);
		
		ManifestacijaDAO.update(ApplicationUI.getConn(), ma);
		System.out.println("Manifestacija je uspesno izmenjena.");
	}
	
	// brisanje manifestacije
	public static void obrisiManifestaciju() {
		Manifestacija ma = pronadjiManifestaciju();
		if (ma != null) {
			ManifestacijaDAO.delete(ApplicationUI.getConn(), ma.getId());
		}
		System.out.println("Manifestacija je obrisana.");
	}




}
