package ui;

import java.util.List;

import dao.GradDAO;
import model.Grad;
import utils.PomocnaKlasa;


public class GradUI {

	public static void prikazSvihGradova() {
		List<Grad> gradovi = GradDAO.getAll(ApplicationUI.getConn());
		System.out.println("\n=============================");
		System.out.printf("%-4s %-8s %13s\n", "ID", "PTT", "NAZIV");
		System.out.println("=============================");
		for (Grad grad : gradovi) {
			System.out.printf("%-4s %-8s %13s\n", grad.getId(), grad.getPttBroj(), grad.getNaziv());
		}
		System.out.println("=============================\n");
	}

	public static void unosNovogGrada() {
		System.out.print("Unesi ptt broj grada:");
		int pttG = PomocnaKlasa.ocitajCeoBroj();
		
		while (pronadjiGradPtt(pttG) != null) {
			System.out.println("Grad sa ptt-om " + pttG
					+ " vec postoji");
			pttG = PomocnaKlasa.ocitajCeoBroj();
		}
		System.out.print("Unesi naziv grada:");
		String nazivG = PomocnaKlasa.ocitajTekst();

		Grad gr = new Grad(0, pttG, nazivG);
		// ovde se moze proveravati i povratna vrednost i onda ispisivati poruka
		GradDAO.add(ApplicationUI.getConn(), gr);
		
	}
	
	// pronadji studenta
	public static Grad pronadjiGrad() {
		Grad retVal = null;
		System.out.print("Unesi naziv grada:");
		String naziv = PomocnaKlasa.ocitajTekst();
		retVal = pronadjiGrad(naziv);
		if (retVal == null)
			System.out.println("Grad pod nazivom " + naziv
					+ " ne postoji u evidenciji");
		return retVal;
	}

	// pronadji studenta
	public static Grad pronadjiGrad(String naziv) {
		Grad retVal = GradDAO.getGradByNaziv(ApplicationUI.getConn(),
				naziv);
		return retVal;
	}
	
	public static Grad pronadjiGradPtt(int ptt) {
		Grad retVal = GradDAO.getGradByPtt(ApplicationUI.getConn(),
				ptt);
		return retVal;
	}
	
	
	public static void promeniGrad() {
	
		System.out.print("Unesi ptt broj grada:");
		int pttG = PomocnaKlasa.ocitajCeoBroj();
		
		while (pronadjiGradPtt(pttG) == null) {
			System.out.println("Grad sa ptt-om " + pttG
					+ " ne postoji");
			pttG = PomocnaKlasa.ocitajCeoBroj();
		}
		Grad gr = pronadjiGradPtt(pttG);
		
		System.out.print("Unesi novi ptt grada:");
		int gradPtt = PomocnaKlasa.ocitajCeoBroj();

		System.out.print("Unesi novi naziv grada:");
		String grNaziv = PomocnaKlasa.ocitajTekst();

		gr.setPttBroj(gradPtt);
		gr.setNaziv(grNaziv);
		
		GradDAO.update(ApplicationUI.getConn(), gr);
		System.out.println("Grad je uspesno izmenjen.");
	}
	
	public static void obrisiGrad() {
		Grad gr = pronadjiGrad();
		if (gr != null) {
			GradDAO.delete(ApplicationUI.getConn(), gr.getId());
		}
		System.out.println("Grad je obrisan.");
	}




	
}
