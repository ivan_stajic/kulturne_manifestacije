DROP SCHEMA IF EXISTS kulturnemanifestacije;
CREATE SCHEMA kulturnemanifestacije DEFAULT CHARACTER SET utf8;
USE kulturnemanifestacije;

CREATE TABLE grad(
	id INT AUTO_INCREMENT,
	pttBroj INT NOT NULL,
	naziv VARCHAR(20) NOT NULL,
	PRIMARY KEY (id)
);

CREATE TABLE manifestacija(
	id INT AUTO_INCREMENT,
	naziv VARCHAR(30) NOT NULL,
    brojPosetilaca INT NOT NULL,
    gradOdrzavanja INT NOT NULL,
	PRIMARY KEY (id),
	
	FOREIGN KEY (gradOdrzavanja) REFERENCES grad(id)
	    ON DELETE RESTRICT
);

INSERT INTO grad (id, pttBroj, naziv) VALUES (1, 23000, 'Zrenjanin');
INSERT INTO grad (id, pttBroj, naziv) VALUES (2, 11000, 'Beograd');
INSERT INTO grad (id, pttBroj, naziv) VALUES (3, 21000, 'Novi Sad');
INSERT INTO grad (id, pttBroj, naziv) VALUES (4, 26300, 'Vrsac');
INSERT INTO grad (id, pttBroj, naziv) VALUES (5, 23300, 'Kikinda');

INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (1, 'Dani Piva', 350000, 1);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (2, 'Beer Fest', 820000, 2);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (3, 'Exit', 620000, 3);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (4, 'Dani grozdja', 230000, 4);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (5, 'Dani ludaje', 190000, 5);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (6, 'Tamburica Fest', 70000, 1);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (7, 'Serbia Fahion Week', 85000, 3);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (8, 'Youth Fair', 23000, 3);
INSERT INTO manifestacija (id, naziv, brojPosetilaca, gradOdrzavanja) VALUES (9, 'FEST', 320000, 2);